// ? Les raccourcis sympas :
// ALT + Z → Wrap automatique sur Visual Studio Code
// ALT + Flèche haut ou Flèche bas → Monte ou descend la ligne selectionnée
// Raccourcis pour le panel emote Touche Windows + ;
// ! Raccourcis pour contrer Aurélien et vérouiller sa session Windows + L
// Commenter rapidement le code : Ctrl + :   ou Ctrl + k + c 


//Better Comments de Aaron Bond
// ? Pour les questions
// * fnrefne
// ! frejfirejfe
// TODO jfreojfore

console.log("Coucou")

//Récupérer un élément
const BTN_SUBMIT = document.getElementById("btn-submit")
const BIENVENUE = document.getElementById("bienvenue")


//Ajouter un évènement
BTN_SUBMIT.addEventListener('click', (event) => {
    event.preventDefault() //Le formulaire n'est plus envoyé en post sur l'action
    console.log("Formulaire envoyé")
    //document.forms -> vous donne un tableau contenant tous les formulaires de la page / même s'il n'y a qu'un seul formulaire, il sera juste dans la première case du tableau
    //document.forms[0] -> On accède au formulaire dans la case 0 du tableau (la première case)
    console.log(document.forms[0].firstname.value)
    const formulaire = document.forms[0]
    BIENVENUE.innerHTML = `Bonjour ${formulaire.firstname.value}, vous avez ${formulaire.age.value} ans. Cuisine a été coché ? ${formulaire.cuisine.checked}. La ville choisie : ${formulaire.country.value}`
    

    // ! Attention, pour les checkbox, la valeur ne sera pas dans value mais dans checked
    // ! Pour les select et les radio, dans value, nous aurons la value renseignée par l'option ou l'input radio
})


